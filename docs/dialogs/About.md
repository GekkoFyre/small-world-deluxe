### [Small World Deluxe](https://git.gekkofyre.io/amateur-radio/small-world-deluxe/) (pre-alpha)

Copyright © 2006 to 2019 – [GekkoFyre Networks](https://gekkofyre.io/), All Rights Reserved.

#### Authors

- [Phobos Aryn'dythyrn D'thorga](https://git.gekkofyre.io/phobos-dthorga) [ Lead Programmer / Author ]
- Kenric D'Souza [ Software Engineering Consultant ]

#### Credits, Contributors & Libraries

Please see [CREDITS](https://git.gekkofyre.io/amateur-radio/dekoder-for-morse/blob/develop/CREDITS) and [CONTRIBUTING.md](https://git.gekkofyre.io/amateur-radio/dekoder-for-morse/blob/develop/CONTRIBUTING.md) just in-case anything is missing below.

`<insert legalese from third-party libraries>`

#### Licensing & Legalese

Please see the [LICENSE](https://git.gekkofyre.io/amateur-radio/dekoder-for-morse/blob/develop/LICENSE) file within our GitLab repository for all licensing and legal matters.

