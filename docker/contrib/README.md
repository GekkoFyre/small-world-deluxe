# Docker Contributions

The following individuals and/or organizations have contributed in some modest (yet important!) way towards the [Small World Deluxe](https://git.gekkofyre.io/amateur-radio/small-world-deluxe/) project, whether it be knowingly or even passively (i.e. such as through a libre license via their published work as found on the Internet). Either way, we owe our thanks to the following people.

------

### Arch Linux

- `docker-mingw-qt5` was sourced from [Docker Hub](https://hub.docker.com/) and created by [Wouter Haffmans](https://github.com/haffmans), which we now use for the [MinGW](http://www.mingw.org/) side of our operations when we require cross-compilation efforts from Linux -> Microsoft Windows.

